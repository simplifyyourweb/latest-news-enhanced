<?php
/**
 * @copyright	Copyright (C) 2011 Simplify Your Web, Inc. All rights reserved.
 * @license		GNU General Public License version 3 or later; see LICENSE.txt
 */

defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Form\FormHelper;

FormHelper::loadFieldClass('list');

/**
 * Author alias selection
 */
class JFormFieldAuthorAliasSelect extends \JFormFieldList
{
	public $type = 'AuthorAliasSelect';

	protected function getOptions()
	{
		$options = array();

		if ($this->option == 'com_k2') {
			\JLoader::register('SYWK2', JPATH_LIBRARIES.'/syw/k2.php');

			if (SYWK2::exists()) {

				$db = Factory::getDbo();

				$query = $db->getQuery(true);
				
				$query->select('DISTINCT created_by_alias');
				$query->select($db->quoteName('created_by_alias', 'value'));
				$query->select($db->quoteName('created_by_alias', 'text'));
				$query->from($db->quoteName('#__k2_items'));
				$query->where($db->quoteName('created_by_alias') . ' != ' . $db->quote(''));
				$query->order('created_by_alias', 'ASC');

				$db->setQuery($query);

				try {
					$authors = $db->loadObjectList();
				} catch (\DatabaseExceptionExecuting $e) {
					$authors = array();
				}

				$options = array_merge($options, $authors);
			}
		} else {

			$db = Factory::getDbo();

			$query = $db->getQuery(true);

			$query->select('DISTINCT created_by_alias');
			$query->select($db->quoteName('created_by_alias', 'value'));
			$query->select($db->quoteName('created_by_alias', 'text'));
			$query->from($db->quoteName('#__content'));
			$query->where($db->quoteName('created_by_alias') . ' != ' . $db->quote(''));
			$query->order('created_by_alias', 'ASC');

			$db->setQuery($query);

			try {
				$authors = $db->loadObjectList();
			} catch (\DatabaseExceptionExecuting $e) {
				$authors = array();
			}

			$options = array_merge($options, $authors);
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}

	public function setup(\SimpleXMLElement $element, $value, $group = null)
	{
		$return = parent::setup($element, $value, $group);

		if ($return) {
			$this->option = isset($this->element['option']) ? $this->element['option'] : '';
		}

		return $return;
	}
}
?>